pub mod def;
pub mod man;

use image::{GenericImageView};

pub fn read_image(path: &str) -> Result<(Vec<u8>, usize, usize), ()> {
    let image_bytes:Vec<u8> = vec![];
    let result_file = image::io::Reader::open(path);
    if result_file.is_ok() {
        let file = result_file.unwrap();
        let mut result_image = file.with_guessed_format().unwrap().decode();
        if result_image.is_ok() {
            let image = result_image.unwrap();
            Ok((image.as_bytes().to_vec(), image.dimensions().0 as usize, image.dimensions().1 as usize))
        } else {
            println!("Error opening file: {}", path);
            Err(())
        }
    } else {
        println!("Error creating reader fot file: {}", path);
        Err(())
    }
}

pub fn canvas_bytes_partitions(pixel_depth: &def::PixelDepth, canvas_size: (usize, usize), partition_count: usize, bytes: &Vec<u8>) -> Vec<Vec<u8>> {
    let mut partitions: Vec<Vec<u8>> = vec![];
    let mut temp_partition: Vec<u8> = vec![];
    let owned_pixel_depth = pixel_depth.clone() as usize;
    let partition_width: usize = canvas_size.0/ partition_count;
    let partition_height: usize = canvas_size.1 / partition_count;
    let partition_offset: Vec<(usize, usize)> = vec![];

    let partition_size: usize = bytes.len()/partition_count;
    let mut current_y: usize = 0;
    
    for _ in 0..partition_count {
        for _ in current_y..(current_y+partition_size) {
            if bytes[current_y] != 0 { 
                temp_partition.push(bytes[current_y]);
            } else {
                temp_partition.push(1);
            }
            current_y+=1;
            if current_y == bytes.len() {
                break;
            }
        }
        partitions.push(temp_partition.clone());        
        temp_partition.clear();
        if current_y == bytes.len() {
            break;
        }
        // } else if use_first_partition_only && partitions.len() > 1 {
        //     break;
        // }
    }

    //let partitions_length = partition_count * partition_height * partition_width * owned_pixel_depth;
    if partition_count > partitions.len() {
        for last_byte in (partition_size*partition_count)..bytes.len(){
            temp_partition.push(bytes[last_byte]);
        }
        if temp_partition.len() > 0 {
            partitions.push(temp_partition.clone());
        }
    }

    partitions
}