# Fotan

Find duplicate or near duplicate images in a directory and copy duplicate pairs to a new sub directory named `Duplicates` (currently not configurable).

### Colouring

Fotan considers two images where the only difference is the colour component to be equal. So a colour version and a black-and-white version of an image are considered duplicates of each other.

### Example - Linux

`fotan -p /home/john/Pictures`

Duplicates will be placed in `/home/john/Pictures/Duplicates`. You must decide which of the duplicates you want to keep and these selected images back into the main images folder that was used to scan for duplicates in.
