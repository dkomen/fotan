use std::io::prelude::*;

pub fn read_dir(path: &str) -> Vec<String> {
    let mut files: Vec<String> = vec![];
    let result_read_dir = std::fs::read_dir(&path);
    if result_read_dir.is_ok() {
        for r in result_read_dir.unwrap() {
            if r.is_ok() {
                let entry = r.unwrap();
                if entry.path().is_file() {
                    files.push(entry.path().to_string_lossy().to_string())
                }
            } else {
                println!("Could not read: {}", r.unwrap_err());
            }
        }
    } else {
        println!("Could not read directory: {}", path);
    }

    files
}

/// Read a files contents and return it as a string
pub fn read_from_file_as_string(path: &str) -> Option<String> {
    let mut file_data = String::new();

    let file_handle = std::fs::File::open(path);
    let mut file_handle = match file_handle {
        Ok(file) => file,
        Err(_) => {
            return None;
        }
    };

    let results = file_handle.read_to_string(&mut file_data);
    let _ = match results {
        Ok(results) => results,
        Err(_error) => {
            
            return None;
        }
    };

    Some(file_data)
}


/// Write a vector to a file
pub fn write_to_file(path: &str, file_data: &Vec<u8>, append_to_file: bool) -> bool {
    if !append_to_file && path_exists(&path) {
        let _ = delete_file(&path);
    }
    let file_handle = std::fs::OpenOptions::new().create(true).write(true).append(append_to_file).open(path);
    let mut file_handle = match file_handle {
        Ok(file) => file,
        Err(error) => {
       
            return false;
        }
    };

    let results = file_handle.write(&file_data);
    let _ = match results {
        Ok(results) => results,
        Err(error) => {

            return false;
        }
    };

    true
}

/// Delete a file from disk
pub fn delete_file(file_path: &str) -> bool {
    if path_exists(&file_path) {
        if std::fs::remove_file(&file_path).is_ok() {
            true
        } else {

            false
        }
    } else {
        // File doesn't exist, so a delete success is reported
        true
    }
}

/// Does a file exist?
pub fn path_exists(path: &str) -> bool {
    let path_to_file_buff = std::path::PathBuf::from(path);
    let path_to_file = path_to_file_buff.to_str().unwrap();

    std::path::Path::new(path_to_file).exists()
}

/// Strip directory name form file path, leaving only file name
pub fn get_only_file_name(path: &str) -> String {
    let path_buff = std::path::PathBuf::from(path);

    path_buff.components().last().unwrap().as_os_str().to_string_lossy().into()
}

/// Create a new directory tree
pub fn create_directory_all(relative: bool, directory_name: &str) -> Result<String, String> {
    if relative {
        let current_directory = std::env::current_dir().unwrap();
        let full_directory_name = current_directory.join(directory_name);
        let result_create = std::fs::create_dir_all(full_directory_name.to_owned());
        if result_create.is_ok() {
            return Ok(full_directory_name.to_str().unwrap().into());
        } else {
            return Err(format!("Could not create directory: {}, Error: {}", directory_name, result_create.unwrap_err()));
        }
    } else {
        if std::fs::create_dir_all(directory_name).is_ok() {
            return Ok(format!("{}", directory_name));
        } else {
            return Err(format!("Could not create directory: {}", directory_name));
        }
    };
}
