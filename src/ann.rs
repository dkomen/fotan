use rand::Rng;
use serde::{Deserialize, Serialize};


#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Neuron {
    pub weight: f64,
    pub value: f64,
    pub output: f64
}
impl Neuron{
    pub fn new() -> Neuron {
        let mut rng = rand::thread_rng();
        Neuron {
            weight: 0.5, //rng.gen(), //TODO
            value: 1_f64,
            output: 0_f64
        }
    }
    pub fn new_input(value: f64) -> Neuron {        
        Neuron {
            weight: 0.5,
            value: value,
            output: 0.0
        }        
    }

    pub fn calculate_output(&mut self, previous_layer: &Vec<Neuron>)  {
        let mut total: f64 = 0.0;
        for parent_neuron in previous_layer.iter() {
            total += parent_neuron.output;
        }
        self.value = total * self.weight;
        self.output = self.value;
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NeuralNetwork {
    pub layers: Vec<Vec<Neuron>>
}
impl NeuralNetwork {
    pub fn new(hidden_layers: u32, neurons_per_hidden_layer: u32, neurons_per_output_layer: u32) -> NeuralNetwork {
        let mut layers: Vec<Vec<Neuron>> = vec![];
        for _ in 0..hidden_layers {
            let mut hidden_layer: Vec<Neuron> = vec![];
            for _ in 0..neurons_per_hidden_layer {
                hidden_layer.push(Neuron::new());
            }
            //hidden_layer.push(Neuron::create_bias(0.0));
            layers.push(hidden_layer);
        }

        let mut output_layer: Vec<Neuron> = vec![];
        for _ in 0..neurons_per_output_layer {
            output_layer.push(Neuron::new());
        }

        layers.push(output_layer);

        NeuralNetwork {
            layers: layers
        }
    }

    /// Returns error delta
    pub fn run(&mut self, input_layer: &mut Vec<Neuron>) -> Vec<f64> {
        for input in 0..input_layer.len() {
            input_layer[input].output = input_layer[input].weight * input_layer[input].value;
        }

        loop {
            for l in 0..self.layers.len() {

                // Calculate outputs for all layers
                for neuron in 0..self.layers[l].len() {
                    if l != 0 {
                        let middle_layer = self.layers[l-1].clone();
                        self.layers[l][neuron].calculate_output(&middle_layer);
                    } else {
                        self.layers[l][neuron].calculate_output(&input_layer);
                    }
                }
            }

            let mut output_layer: Vec<f64> = vec![];
            for neuron in 0..self.layers[self.layers.len()-1].len() {
                let current_layer = self.layers.len()-1;
                output_layer.push(self.layers[self.layers.len()-1][neuron].output);
            }
            return output_layer;
        }
    }

    /// Convert a byte vector to a vector of input neurons
    pub fn bytes_to_input_neurons(bytes: &Vec<u8>) -> Vec<Neuron> {
        let mut neurons: Vec<Neuron> = vec![];
        for b in 0..bytes.len() {
            neurons.push(Neuron::new_input(bytes[b] as f64));
        }

        neurons
    }
    
    /// Convert a floating point vector to a vector of input neurons
    pub fn floats_to_input_neurons(floats: &Vec<f32>,) -> Vec<Neuron> {
        let mut neurons: Vec<Neuron> = vec![];
        for f in 0..floats.len() {
            neurons.push(Neuron::new_input(floats[f] as f64));
        }

        neurons
    }
}
