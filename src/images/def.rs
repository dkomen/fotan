#[derive(Clone)]
pub enum PixelDepth {
    Bits8=1,
    BitsRgb=3,
    BitsRgba=4,
}

#[derive(Clone)]
pub struct Pixel {
    pub depth: PixelDepth,
    pub r: i16,
    pub g: i16,
    pub b: i16,
    pub a: i16
}
impl Pixel {
    pub fn new(depth: PixelDepth) -> Pixel {
        Pixel {
            depth: depth,
            r: 0,
            g: 0,
            b: 0,
            a: 0
        }
    }
    pub fn new_rgb(depth: PixelDepth, r: i16, g: i16, b: i16) -> Pixel {
        Pixel {
            depth: depth,
            r: r,
            g: g,
            b: b,
            a: 0
        }
    }

    pub fn new_rgba(depth: PixelDepth, r: i16, g: i16, b: i16, a: i16) -> Pixel {
        Pixel {
            depth: depth,
            r: r,
            g: g,
            b: b,
            a: a
        }
    }

    pub fn from_bytes(depth: PixelDepth, bytes: &Vec<u8>) -> Pixel {
        let mut new_pixel = Pixel::new_rgba(depth.to_owned(), 0, 0, 0, 0);
        match depth {
            PixelDepth::Bits8 => new_pixel.r = bytes[0] as i16,
            PixelDepth::BitsRgb => { new_pixel.r=bytes[0] as i16; new_pixel.g=bytes[1] as i16; new_pixel.b=bytes[2] as i16; },
            PixelDepth::BitsRgba => { new_pixel.r=bytes[0] as i16; new_pixel.g=bytes[1] as i16; new_pixel.b=bytes[2] as i16; new_pixel.a=bytes[3] as i16; },
        }

        new_pixel
    }

    pub fn to_bytes(&self, depth: PixelDepth) -> Vec<u8> {
        let mut bytes: Vec<u8> = vec![];
        match depth {
            PixelDepth::Bits8 => bytes.push(self.r as u8),
            PixelDepth::BitsRgb => { bytes.push(self.r as u8); bytes.push(self.g as u8); bytes.push(self.b as u8); },
            PixelDepth::BitsRgba => { bytes.push(self.r as u8); bytes.push(self.g as u8); bytes.push(self.b as u8); bytes.push(self.a as u8); },
        }

        bytes
    }

    pub fn increment(&mut self, increment_by: &i16) {
        if *increment_by < 0_i16 && self.r > *increment_by { self.r -= increment_by }
        else if *increment_by > 0_i16 && self.r < 255 - *increment_by { self.r += increment_by }

        if *increment_by < 0_i16 && self.g > *increment_by { self.g -= increment_by }
        else if *increment_by > 0_i16 && self.g < 255 - *increment_by { self.g += increment_by }

        if *increment_by < 0_i16 && self.r > *increment_by { self.r -= increment_by }
        else if *increment_by > 0_i16 && self.b < 255 - *increment_by { self.b += increment_by }
    }

    pub fn luminance(&self) -> u8 {
        let r: f32 = self.r as f32/255_f32;
        let g: f32 = self.g as f32/255_f32;
        let b: f32  = self.b as f32/255_f32;
        let min = self.get_min_value(&r, &g, &b);
        let max = self.get_max_value(&r, &g, &b);

        (((min + max) / 2_f32) * 100_f32) as u8
    } 

    pub fn hue(&self) -> f32 {
        let r: f32 = self.r as f32/255_f32;
        let g: f32 = self.g as f32/255_f32;
        let b: f32  = self.b as f32/255_f32;
        let min = self.get_min_value(&r, &g, &b);
        let max = self.get_max_value(&r, &g, &b);

        let mut hue = 0_f32;
        if r >= g && r >= b { 
            hue = (g - b) / (max-min)
        } else if g >= r && g >= b 
        { 
            hue = 2_f32 + (b - r) / (max-min)
        } else 
        { 
            hue = 4_f32 + (r - g) / (max-min);
        }

        hue
    } 

    fn get_min_value(&self, r: &f32, g: &f32, b: &f32) -> f32 {
        if r <= g && r <= b { *r }
        else if g <= r && g <= b { *g }
        else { *b }
    }
    
    fn get_max_value(&self, r: &f32, g: &f32, b: &f32) -> f32 {
        if r >= g && r >= b { *r }
        else if g >= r && g >= b { *g }
        else { *b }
    }
}