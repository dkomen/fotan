use std::thread;
use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};
use serde::{Deserialize, Serialize};

mod ann;
mod images;
mod file;
//mod rust_cli_arguments;

fn main() {    
    let mut error_message: String = String::new();
    let mut cli_args = rust_cli_arguments::Arguments::new();
    //^[0-9a-zA-Z]{{6,{}} - alphanumeric
    cli_args.register("-p", "", "^[ -~]*$", Some("Argument '-p' is not a valid path string".into())); //path to directory with images
    cli_args.register("-t", &num_cpus::get().to_string(), "^[0-9]{1,3}$", Some("Argument '-t' must be an integer between 1 and X: Threads".into())); //threads
    cli_args.register("-a", "60", "^[0-9]{1,2}$", Some("Argument '-a' must be an integer between 1 and 99".into()));
    cli_args.register("-c", "2", "^[0-9]{1,2}$", Some("Argument '-c' must be an integer between 1 and 99: Partitions".into()));
    cli_args.register("-l", "2", "^[0-9]{1,2}$", Some("Argument '-l' must be an integer between 1 and 3: Level of accuracy".into()));
    cli_args.register("-e", "4.5", "^[0-9]{1,2}$", Some("Argument '-e' must be an integer between 1 and 99: Max number of errors".into()));
    let result_cli_args = cli_args.get_from_command_line();
    if result_cli_args.is_ok() {
        let cli_args = result_cli_args.unwrap();
        let option_path = cli_args.get_argument_value("-p");
        if option_path.is_some() {
            let option_level_of_accuracy = cli_args.get_argument_value("-l");
            let mut partitions: usize = cli_args.get_argument_value("-c").unwrap().parse::<usize>().unwrap();
            let mut accuracy_error_rate: usize = cli_args.get_argument_value("-a").unwrap().parse::<usize>().unwrap();
            let mut max_error = cli_args.get_argument_value("-e").unwrap().parse::<f64>().unwrap();
            if option_level_of_accuracy.is_some() {
                let level_of_accuracy = option_level_of_accuracy.unwrap();
                if level_of_accuracy.parse::<usize>().unwrap()==1 {
                    partitions = 12;
                    accuracy_error_rate=66;
                    max_error=4 as f64;
                } else if level_of_accuracy.parse::<usize>().unwrap()==3 {
                    partitions = 6;
                    accuracy_error_rate=50;
                    max_error=8 as f64;
                } else if level_of_accuracy.parse::<usize>().unwrap()!=2 {
                    error_message = "Command-line argument for level of accuracy ('-l') was not supplied".into();
                }
            }

            let max_threads = cli_args.get_argument_value("-t").unwrap().parse::<usize>().unwrap();
            if max_threads >= 1 && max_threads <= (2 as usize).pow(16) {
                println!("Using {} threads", max_threads);
                run(partitions, &option_path.unwrap(), accuracy_error_rate, max_error, max_threads);
            } else {
                error_message = "Command-line argument for number of threads to use ('-t') must be a u16 number of value 1 or greater".into();
            }
        } else {
            error_message = "Command-line argument for path to the image files to scan ('-p') was not supplied".into();
        }
    } else {
        error_message = "Command-line arguments are not valid or are incomplete".into();
    }

    if error_message!=String::new() {
        println!(" _____     _             ");
        println!("|  ___|__ | |_ __ _ _ __ ");
        println!("| |_ / _ \\| __/ _` | '_ \\ ");
        println!("|  _| (_) | || (_| | | | |");
        println!("|_|  \\___/ \\__\\__,_|_| |_|");
        println!();
        println!("{}", error_message);
    }
}
#[derive(Clone, Debug, Serialize, Deserialize)]
struct Hash {
    file_name: String,
    hashes_partial: Vec<f64>,
    hashes_full: Vec<f64>,
    ratio: f32
}
impl Hash {
    fn new(file_name: String, hashes_partial: Vec<f64>, ratio: f32) -> Hash {
        Hash {
            file_name: file_name,
            hashes_partial: hashes_partial,
            hashes_full: vec![],
            ratio: ratio
        }
    }
}

struct FilesToThread {
    pub file_names: Vec<(usize, Vec<String>)>
}
impl FilesToThread {
    pub fn new() -> FilesToThread {
        FilesToThread {
            file_names: vec![]
        }
    }

    pub fn add(&mut self, index: usize, file_name: &str) {
        for i in self.file_names.iter() {
            if i.0 == index {                
                self.file_names[index].1.push(file_name.into());
                return;
            }
        }

        //Not found so add it
        self.file_names.push((index, vec![file_name.into()]));
    }

    pub fn get(&self, index: usize) -> Vec<String> {
        for i in self.file_names.iter() {
            if i.0 == index {                
                return self.file_names[index].1.clone();
            }
        }

        return vec![];
    }
}

fn get_hash(file_path:&str, ann: &mut ann::NeuralNetwork, partitions_count: usize) -> Option<Hash> {
    let result_image = images::read_image(&file_path);
    if result_image.is_ok() {
        let image = result_image.unwrap();                    
        let canvas_size = (image.1, image.2);
        let image_partitions = images::canvas_bytes_partitions(&images::def::PixelDepth::BitsRgb, canvas_size, partitions_count, &image.0);
        let mut ann_results: Vec<f64> = vec![];
        
        for partition in image_partitions {
            let mut neurons = ann::NeuralNetwork::bytes_to_input_neurons(&partition);
            let ann_result = ann.run(&mut neurons);
            for result in ann_result {
                ann_results.push(result);
            }
        }

        let hash: Hash = Hash::new(file_path.into(), ann_results, canvas_size.0 as f32/canvas_size.1 as f32);

        Some(hash)

    } else {
       println!("Error opening: {}", file_path);

       None
    }
    
}

/// Load two files and compare their hashes
fn compare_new_hashes(file_hashes: &mut Vec<Hash>, index_a: usize, index_b: usize, ann: &mut ann::NeuralNetwork, leniancy: f64, max_error: f64, path: &str) -> bool {
    let partitions_count = 50;
    let hasher_a = file_hashes[index_a].clone();
    let hasher_b = file_hashes[index_b].clone();
    let hash_a = match hasher_a.hashes_full.len() > 0 {  
        true => hasher_a.hashes_full,
        false => {            
            let option = get_hash(&format!("{}/{}", path, hasher_a.file_name), ann, partitions_count.to_owned());
            if option.is_some() {
                file_hashes[index_a].hashes_full = option.unwrap().hashes_partial;
            }
            file_hashes[index_a].clone().hashes_full
        }
    };

    let hash_b = match hasher_b.hashes_full.len() > 0 {  
        true => hasher_b.hashes_full,
        false => {
            let option = get_hash(&format!("{}/{}", path, hasher_b.file_name), ann, partitions_count.to_owned());
            if option.is_some() {
                file_hashes[index_b].hashes_full = option.unwrap().hashes_partial;
            }
            file_hashes[index_b].clone().hashes_full
        }
    };

    return compare_hashes(&hash_a, &hash_b, leniancy, max_error);
}

/// Find if two hashes are similar enough so as to be considered equal
fn compare_hashes(hashes_a: &Vec<f64>, hashes_b: &Vec<f64>, leniancy: f64, max_error:f64) -> bool {
    let mut error_percentage: Vec<f64> = vec![];
                            
    for result_index in 0..hashes_b.len() {
        let ratio = match hashes_a[result_index] < hashes_b[result_index] { 
            true => (hashes_a[result_index]+1.0) / (hashes_b[result_index]+1.0),
            false => (hashes_b[result_index]+1.0) / (hashes_a[result_index]+1.0)    
        } * 100_f64;
        if ratio < 1.0 { 
            return false; 
        } else {
            error_percentage.push(ratio);
        }
    }

    //if error_percentage.len() as f64 > hash_a.ann_results.len() as f64 * 0.025_f64 { //More than 2.5% are errors
        let average_error_ratio: f64 = error_percentage.iter().sum::<f64>() / error_percentage.len() as f64;
        let mut failed_counter = 0;
        for index in 0..error_percentage.len() {
            let error = 100.0 * (1.0 - match error_percentage[index]< average_error_ratio { 
                true => error_percentage[index] / average_error_ratio,
                false => average_error_ratio / error_percentage[index]
            });
            if error > max_error*leniancy {  
                failed_counter+=1;
                if failed_counter > 1 {                                                      
                    return false;
                }
            }
        }
    //}

    true
}

fn run(partitions: usize, path: &str, accuracy_error_rate: usize, max_error: f64, max_threads: usize) {
    let start = std::time::Instant::now();
    
    let option_file_hashes = file::read_from_file_as_string(&format!("{}/hashes.json", path));
    let mut file_hashes: Vec<Hash> = match option_file_hashes.is_some() { 
        true => serde_json::from_str::<Vec<Hash>>(&option_file_hashes.unwrap()).unwrap(),
        false => vec![]
    };

    let ann = ann::NeuralNetwork::new(1, partitions as u32, 1);
    let files: Vec<String> = file::read_dir(path);
    let mut file_counter: usize = 0;
    println!("Preparing hashes...");

    let mut threads = FilesToThread::new();
    let concurrency = max_threads;
    
    let cloned = files.clone();
    let mut current_thread = 0;
    for file_index in 0..cloned.len() {
        threads.add(current_thread,&cloned[file_index]);
        current_thread+=1;
        if current_thread == concurrency {
            current_thread = 0;
        }
    }

    let mut bar = pbr::ProgressBar::new(100);
    bar.message(&format!("{} ", "Progress (%)"));
    //bar.bar_display.format("╢▌▌ ╟");
    bar.show_time_left=true;
    bar.show_percent = false;
    bar.show_speed=false;
    bar.format("[=> ]");

    let mut handles: Vec<thread::JoinHandle<()>> = vec![];
    let (tx_send_hash, rx_receive_hash): (Sender<Hash>, Receiver<Hash>) = mpsc::channel();
    let mut active_threads_counter: usize=0;
    let (tx_send_complete, rx_complete): (Sender<bool>, Receiver<bool>) = mpsc::channel();
    for file in files.iter() {
        //println!("Processing: {}", file);
        if !file.to_lowercase().ends_with("xmp") { //xmp: darktable sidecar file
            if file.to_lowercase().ends_with("json") == false {
                file_counter += 1;
                let option_file_retrieved_hash = file_hashes.clone().into_iter().find(|h| &format!("{}/{}", path, h.file_name) == file);
                if option_file_retrieved_hash.is_none() {
                    let (tx_send_file, rx_receive_file): (Sender<(usize, String)>, Receiver<(usize, String)>) = mpsc::channel();
                    let tx_thread_send_hash = tx_send_hash.clone();
                    let tx_thread_send_complete = tx_send_complete.clone();
                    let mut thread_ann = ann.clone();
                    let handle = thread::spawn(move || {
                        //////////////////
                        let thread_file = rx_receive_file.recv().unwrap();
                        let file = thread_file.1;
                        let option_hash = get_hash(&file, &mut thread_ann, partitions);
                        if option_hash.is_some() {
                            let mut hash = &mut option_hash.unwrap().clone();
                            let file_name = file::get_only_file_name(&hash.file_name);
                            hash.file_name = file_name;
                            let _ = tx_thread_send_hash.send(hash.to_owned());
                        }
                        let _ = tx_thread_send_complete.send(true);
                        //////////////////
                    });
                    active_threads_counter += 1;
                    handles.push(handle);
                    let _ = tx_send_file.send((0, file.to_owned()));

                    loop {
                        let result_completed = rx_complete.try_recv();
                        if result_completed.is_ok() {
                            let result_hashes = rx_receive_hash.try_recv();
                            if result_hashes.is_ok() {
                                file_hashes.push(result_hashes.unwrap());
                                active_threads_counter -= 1;
                                bar.set(((file_counter as f32 / files.len() as f32) * 100.0) as u64);
                            }
                            break;
                        } else if active_threads_counter < concurrency {
                            break;
                        }
                    }
                }
            }
        }
    }

    loop {
        let result_hashes = rx_receive_hash.try_recv();
        if result_hashes.is_ok() {
            file_hashes.push(result_hashes.unwrap());
            active_threads_counter-=1;
            println!("Threads still running: {}", active_threads_counter);
        }
        if active_threads_counter == 0 { 
            break;
        }
    }

    for handler in handles {
        handler.join().unwrap();
    };

    println!("Elapsed: {} seconds", start.elapsed().as_secs());
    println!();

    println!("Start comparing hashes...");
    bar.is_finish=true;
    file_counter = 0;
    
    let mut bar = pbr::ProgressBar::new(100);
    bar.message(&format!("{} ", "Progress (%)"));
    //bar.bar_display.format("╢▌▌ ╟");
    bar.show_time_left=true;
    bar.show_percent = false;
    bar.show_speed=false;
    bar.format("[=> ]");

    let mut duplicates_counter=0;
    let duplicates_path = String::from("Duplicates");
    let mut compared_with: Vec<(usize, Vec<usize>)> = vec![];
    let hashes_length = file_hashes.len();
    let mut hashes_no_longer_needed_in_hash_file: Vec<Hash> = vec![];
    for hash_index in 0..hashes_length {
        file_counter+=1;
        //println!("FILE {} of {}", file_counter, files.len());
        bar.set(((file_counter as f32 /hashes_length as f32)*100.0) as u64);
        let current_hash = file_hashes[hash_index].clone();
        compared_with.push((hash_index, vec![]));

        for hash_index_to_test in 0..hashes_length {
            if hash_index_to_test != hash_index {
                let leniancy = (100_f64 - accuracy_error_rate as f64)/100_f64;
                let h = file_hashes[hash_index_to_test].clone();
                if !(hash_index_to_test<= compared_with.len()-1 && compared_with[hash_index_to_test].1.iter().find(|o|**o==hash_index).is_some()) {                
                    compared_with[hash_index].1.push(hash_index_to_test);
                    
                    if compare_hashes(&current_hash.hashes_partial, &h.hashes_partial, leniancy, max_error) {
                        if compare_new_hashes(&mut file_hashes, hash_index, hash_index_to_test, &mut ann.clone(), leniancy, max_error, path) {
                            let _ = std::fs::create_dir(format!("{}/{}", path, duplicates_path));
                            let new_file1 = std::path::PathBuf::from(h.file_name.clone());
                            let new_file2 = std::path::PathBuf::from(current_hash.file_name.clone());

                            let from_file = format!("{}/{}", path, h.file_name.clone());
                            if std::path::Path::new(&from_file).exists() {
                                duplicates_counter+=1;
                                let _ = std::fs::rename(from_file, format!("{}/{}/{}-{}", path, duplicates_path, duplicates_counter, new_file1.file_name().unwrap().to_str().unwrap()));
                                let _ = std::fs::remove_file(format!("{}/{}", path, h.file_name.clone()));

                                let from_file_duplicate = format!("{}/{}", path, current_hash.file_name.clone());
                                if std::path::Path::new(&from_file_duplicate).exists() {
                                    let _ = std::fs::rename(from_file_duplicate, format!("{}/{}/{}-{}", path, duplicates_path, duplicates_counter, new_file2.file_name().unwrap().to_str().unwrap()));
                                    let _ = std::fs::remove_file(format!("{}/{}", path, current_hash.file_name.clone()));
                                }
                            } else {
                                //Remove hash from hash file as the image no longer exists
                                hashes_no_longer_needed_in_hash_file.push(h)
                            }
                        }
                    }
                }
            };
        };
    }

    for hash_to_remove in hashes_no_longer_needed_in_hash_file.iter_mut() {
        file_hashes.retain(|f|f.file_name!=hash_to_remove.file_name);
    }

    bar.is_finish=true;
    println!();
    println!("Found {} duplicates in {} seconds", duplicates_counter, start.elapsed().as_secs());
    let serialised = serde_json::to_string(&file_hashes).unwrap();
    let _ = file::delete_file(&format!("{}/hashes.json", path));
    let is_saved = file::write_to_file(&format!("{}/hashes.json", path), &serialised.as_bytes().to_vec(), false);
}