
use super::def::*;

pub fn get_average_pixel(pixel_depth: &PixelDepth, bytes: &Vec<u8>) -> Pixel {
    let mut offset = 0_usize;
    let bytes_lenth = bytes.len() as u32;
    let mut total_r: u32 = 0;
    let mut total_g: u32 = 0;
    let mut total_b: u32 = 0;
    let mut total_a: u32 = 0;
    let owned_pixel_depth = pixel_depth.clone() as u32;

    for b in 0..(bytes.len() / owned_pixel_depth as usize) {
        let _ = match pixel_depth {
            PixelDepth::Bits8 => total_r += bytes[b] as u32,
            PixelDepth::BitsRgb => { total_r += bytes[offset] as u32; total_g += bytes[offset+1] as u32; total_b += bytes[offset+2] as u32 },
            PixelDepth::BitsRgba => { total_r += bytes[offset] as u32; total_g += bytes[offset+1] as u32; total_b += bytes[offset+2] as u32; total_a += bytes[offset+3] as u32 }
        };

        offset+=owned_pixel_depth as usize;
    }

    total_r = total_r / (bytes_lenth / owned_pixel_depth);
    total_g = total_g / (bytes_lenth / owned_pixel_depth);
    total_b = total_b / (bytes_lenth / owned_pixel_depth);
    total_a = total_a / (bytes_lenth / owned_pixel_depth);

    Pixel::new_rgba(pixel_depth.to_owned(), total_r as i16, total_g as i16, total_b as i16, total_a as i16)
}

/// Contrast is not added to an alpha channel
pub fn add_contrast (iterations: &u8, pixel_depth: &PixelDepth, bytes: &Vec<u8>) -> Vec<u8> {
    let average_pixel = get_average_pixel(pixel_depth, bytes);
    let average_pixel_hue = average_pixel.hue();
    let mut working_pixel = Pixel::new(pixel_depth.to_owned());
    let mut new_image: Vec<u8> = vec![];
    let owned_pixel_depth = pixel_depth.clone() as usize;
    
    for iteration in 0..*iterations {
        let mut index = 0_usize;
        for _ in 0..(bytes.len()/owned_pixel_depth) {
            working_pixel = Pixel::from_bytes(pixel_depth.to_owned(), &bytes[index..index+owned_pixel_depth].to_vec());
            match pixel_depth {
                PixelDepth::Bits8 => 
                    working_pixel.r += working_pixel.r - average_pixel.r,
                PixelDepth::BitsRgb | PixelDepth::BitsRgba => {
                    if working_pixel.hue() > average_pixel_hue {
                        working_pixel.increment(&1);
                    } else if working_pixel.hue() < average_pixel_hue {
                       working_pixel.increment(&-1);
                    }
                }
            }
            new_image.append(&mut working_pixel.to_bytes(pixel_depth.clone()));
            index += owned_pixel_depth;
        }
    }

    new_image
}

pub fn convert_rgb_to_hue_map(pixel_depth: &PixelDepth, bytes: &Vec<u8>) -> Vec<f32> {
    let mut new_vec: Vec<f32> = vec![];
    let mut working_pixel = Pixel::new(pixel_depth.to_owned());
    let owned_pixel_depth = pixel_depth.clone() as usize;
    let mut index = 0_usize;

    for _ in 0..(bytes.len()/owned_pixel_depth) {
        working_pixel = Pixel::from_bytes(pixel_depth.to_owned(), &bytes[index..index+owned_pixel_depth].to_vec());
        match pixel_depth {
            PixelDepth::BitsRgb | PixelDepth::BitsRgba => {
                new_vec.push(working_pixel.hue().clone());
            },
            _ => ()
        }
        index += owned_pixel_depth;
    };

    new_vec
}

pub fn convert_rgb_to_luminance_map(pixel_depth: &PixelDepth, bytes: &Vec<u8>) -> Vec<u8> {
    let mut new_vec: Vec<u8> = vec![];
    let mut working_pixel = Pixel::new(pixel_depth.to_owned());
    let owned_pixel_depth = pixel_depth.clone() as usize;
    let mut index = 0_usize;

    for _ in 0..(bytes.len()/owned_pixel_depth) {
        working_pixel = Pixel::from_bytes(pixel_depth.to_owned(), &bytes[index..index+owned_pixel_depth].to_vec());
        match pixel_depth {
            PixelDepth::BitsRgb | PixelDepth::BitsRgba => {
                new_vec.push(working_pixel.luminance().clone());
            },
            _ => ()
        }
        index += owned_pixel_depth;
    };

    new_vec
}